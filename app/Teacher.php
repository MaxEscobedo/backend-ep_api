<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'name','date_of_birth','picture','studies','information','enlaced'
    ];

}
