<?php

namespace App\Http\Controllers;
use App\Comment;
use App\Helpers\General;
use App\Subject;
use App\SubjectTeacher;
use App\Teacher;
use App\User;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function index(Request $request)
    {
        $datesRequest = $request->all();
        $dates = [];
        $comments = Comment::all();
        foreach ($comments as $comentario) {
            if($comentario->id_subject==$datesRequest['idSubject']&&$comentario->id_teacher==$datesRequest['idTeacher'])
            {
                $dates['comments'][]=[
                    'comment'=>$comentario['comment'],
                    'userName'=>User::find($comentario['id_user'])->name
                ];
            }
        }
        $dates['subject'] = Subject::find($datesRequest['idSubject']);
        $dates['teacher'] = Teacher::find($datesRequest['idTeacher']);
        foreach (SubjectTeacher::where('id_teacher', $datesRequest['idTeacher'])->get() as $subject){
            $dates['subjects'][]=
                 Subject::find($subject['id_subject']);
        }

        return General::makeResponse(['message' => 'Acción realizada con éxito', 'dates'=>$dates], 200, true);
    }

    public function commentsTeacher(Request $request)
    {
        $date=$request->all();
        $comentarios = [];
        $a = Comment::all();
        foreach ($a as $comentario) {
            if($comentario->id_subject==$date['id_subject']&&$comentario->id_teacher==$date['id_teacher']){
                $comentarios['comentarios'][]= [
                        $comentario
                    ];
            }
        }
        if(!$comentarios)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','Comentarios'=>$comentarios], 200, true);
    }


    public function store(Request $request)
    {
        $data = $request->all();
       
        $comment = Comment::create($data);

        if (!$comment)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);

    }

    public function likes(Request $request)
    {
        $dates = $request->all();
        $teacher = SubjectTeacher::where('id_teacher',$dates['idTeacher'])->where('id_subject',$dates['idSubject'])->get();
        $negative_votes = $teacher[0]->negative_vote;
        $positive_vote = $teacher[0]->positive_vote;
        unset($teacher['id']);

        if($dates['positive_vote']) {
            $positive_vote=$positive_vote+1;
            SubjectTeacher::where('id_teacher',$dates['idTeacher'])->where('id_subject',$dates['idSubject'])->update(['positive_vote'=>$positive_vote]);
        }else{
            $negative_votes++;
            SubjectTeacher::where('id_teacher',$dates['idTeacher'])->where('id_subject',$dates['idSubject'])->update(['negative_vote'=>$negative_votes]);
        }

        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);

    }

}