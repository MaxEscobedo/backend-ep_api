<?php

namespace App\Http\Controllers;

use App\Helpers\General;
use App\Subject;
use App\SubjectTeacher;
use App\Teacher;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{

    public function index()
    {
        $subjects = Subject::all();
        if(!$subjects)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','subjects'=>$subjects], 200, true);
    }


    public function store(Request $request)
    {
        $data=$request->all();
        Subject::create($data);

        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);
    }

    public function showWhitParams(Request $request)
    {
        $dates = $request->all();
        $subjects = [];
        if($dates['id']!=null){
            $subjects = [];
            $subject=Subject::find($dates['id']);
            $subjectTeacher = SubjectTeacher::all();
            $subjects['subject']=[
                $subject
            ];
            $teachers = Teacher::all();
            $votesNegatives=0;
            $votesPositives=0;

            $prof = '';
            foreach ($teachers as $teacher) {
                foreach ($subjectTeacher as $subjectT) {
                    if($subjectT->id_subject == $dates['id'])
                        if($subjectT->id_teacher == $teacher->id) {
                            $prof=Teacher::find($subjectT->id_teacher);
                            $votesNegatives+=$subjectT->negative_vote;
                            $votesPositives+=$subjectT->positive_vote;

                        }

                }

                if($prof != '')
                    if($prof->name==$teacher->name){
                        $subjects['teachers'][]=[
                            'id'=>$prof->id,
                            'name'=>$prof->name,
                            'positive_vote'=>$votesPositives,
                            'negative_vote'=>$votesNegatives
                        ];
                    }
                $prof = '';

                $votesNegatives=0;
                $votesPositives=0;
            }

            

        }else if ($dates['name']=='')
        {
            return $this->index();

        }else{
            $subject =  Subject::where('name', $dates['name'])
                ->orWhere('name', 'like', '%' . $dates['name'] . '%')->get();
            $subjects['subjects'] = $subject;
        }

        if(!$subjects)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','subjects'=>$subjects], 200, true);

    }


    public function show($id)
    {

        $subjects = [];
        $subject=Subject::find($id);
        $subjectTeacher = SubjectTeacher::all();
        $subjects['subject'][]=[
            $subject
        ];
        foreach ($subjectTeacher as $subject) {
            if($subject->id_subject==$id)
                $subjects['teachers'][]=[
                    Teacher::find($subject->id_teacher)
                ];
        }

        if(!$subjects)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','subjects'=>$subjects], 200, true);
    }


    public function destroy($id)
    {
        //
    }
}
