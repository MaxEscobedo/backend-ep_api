<?php

namespace App\Http\Controllers;

use App\Helpers\General;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $dates = $request->all();
        $password = Hash::make($dates['password']);
        $dates['password'] = $password;
        $user = User::create($dates);

        return General::makeResponse(['message' => 'Accion realizada con exito', 'user' => $user], 200, true);
    }

    public function login(Request $request)
    {
        $dates = $request->all();
        $users = User::all();
        foreach($users as $user){
            if(Hash::check($dates['password'],$user->password)&&$dates['email']==$user->email){
                return General::makeResponse(['message' => 'Accion realizada con exito', 'user' => $user], 200, true);
            }
        }
        return General::makeResponse(['message' => 'No se pudo realizar la acción'], 200, true);
    }

}