<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects_teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_teacher');
            $table->unsignedBigInteger('id_subject');
            $table->text('information_subject');
            $table->unsignedBigInteger('positive_vote');
            $table->unsignedBigInteger('negative_vote');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_teacher')->references('id')->on('teachers');
            $table->foreign('id_subject')->references('id')->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects_teachers');
    }
}
